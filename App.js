import React, {useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import Profile from './src/components/Profile';
import Home from './src/components/Home';
import Email from './src/components/Email';

import TriosBottomBar from './src/screens/CurvedBottomBar';

const App = () => {
  return (
    <NavigationContainer>
      <TriosBottomBar
        useBottomText={true}
        EmailText={'Email'}
        HomeText={'Home'}
        ProfileText={'Profile'}
        tab3Image={require('./src/assets/user.png')}
        tab2Image={require('./src/assets/home.png')}
        tab1Image={require('./src/assets/email.png')}
        tab1Component={<Profile />}
        tab2Component={<Home />}
        tab3Component={<Email />}
        bottomBarItemStyle={''}
        bottomBarImageStyle={''}
        BottomTextStyle={''}
        bottomBarStyle={''}
        centerBtnStyle={''}
      />
    </NavigationContainer>
  );
};

export default App;
