// import React, {useState} from 'react';
// import {View, TouchableOpacity, StyleSheet, Image, Text} from 'react-native';

// const BottomBar = ({
//   tab1Image,
//   tab1ActiveImage,
//   tab2Image,
//   tab2ActiveImage,
//   tab3Image,
//   tab3ActiveImage,
//   tab4Image,
//   tab4ActiveImage,
//   tabIconWrapperStyle,
//   iconContainerStyle,
//   containerStyle,
//   tab1Component,
//   tab2Component,
//   tab3Component,
//   tab4Component,
// }) => {
//   const [activeTab, setActiveTab] = useState('home');
//   const [homeActive, setHomeActive] = useState(false);

//   const [htmlState, setHtmlState] = useState('');

//   const onhandle = html => {
//     switch (html) {
//       case 'home':
//         return setHtmlState(tab1Component);
//       case 'search':
//         return setHtmlState(tab2Component);
//       case 'email':
//         return setHtmlState(tab3Component);
//       case 'setting':
//         return setHtmlState(tab4Component);
//       default:
//         setHtmlState('home');
//     }
//     return setHtmlState;
//   };
//   console.log(htmlState, 'htmlState===>');

//   const handleHomePress = () => {
//     setActiveTab('home');
//     setHomeActive(true);
//     onhandle('home');
//     setTimeout(() => {
//       setHomeActive(false);
//     }, 300);
//     // navigation.navigate('Home');
//   };
//   const handleNotificationsPress = () => {
//     setActiveTab('search');
//     // navigation.navigate('Profile');
//     onhandle('search');
//   };
//   const handleProfilePress = () => {
//     setActiveTab('email');
//     // navigation.navigate('Profile')
//     onhandle('email');
//   };
//   const handleEmailPress = () => {
//     setActiveTab('setting');
//     // navigation.navigate('Profile');
//     onhandle('setting');
//   };

//   const renderIcon = (tabName, icon, activeIcon, isActiveTab) => {
//     const activeStyle = isActiveTab ? styles.activeIcon : {};

//     return (
//       <>
//         <Text>{htmlState}</Text>
//         <View style={[styles.iconWrapper, tabIconWrapperStyle]}>
//           <TouchableOpacity
//             onPress={() => {
//               if (tabName === 'home') handleHomePress();
//               else if (tabName === 'search') handleNotificationsPress();
//               else if (tabName === 'email') handleProfilePress();
//               else if (tabName === 'setting') handleEmailPress();
//             }}>
//             <View
//               style={[
//                 styles.iconContainer,
//                 iconContainerStyle,
//                 activeTab === tabName && {backgroundColor: '#FFFFFF40'},
//                 tabName === 'home' && homeActive && styles.homeActive,
//               ]}>
//               <Image
//                 source={activeTab === tabName ? activeIcon : icon}
//                 style={[styles.icon, activeStyle]}
//               />
//             </View>
//           </TouchableOpacity>
//           <Text style={styles.tabName}>{tabName}</Text>
//         </View>
//       </>
//     );
//   };

//   return (
//     <View style={[styles.container, containerStyle]}>
//       {renderIcon(
//         'home',

//         tab1Image,
//         tab1ActiveImage,
//         activeTab === 'home',
//       )}
//       {renderIcon(
//         'search',

//         tab2Image,
//         tab2ActiveImage,
//         activeTab === 'search',
//       )}
//       {renderIcon(
//         'email',

//         tab3Image,
//         tab3ActiveImage,
//         activeTab === 'email',
//       )}
//       {renderIcon(
//         'setting',

//         tab4Image,
//         tab4ActiveImage,
//         activeTab === 'setting',
//       )}
//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   container: {
//     flexDirection: 'row',
//     justifyContent: 'center',
//     backgroundColor: '#2196F3',
//     padding: 6,
//     position: 'absolute',
//     bottom: 0,
//     borderRadius: 30,
//     margin: 10,
//     // height: 50,
//     // marginTop: 10,
//   },
//   iconWrapper: {
//     alignItems: 'center',
//     // justifyContent: 'center',
//     flex: 1,
//   },
//   icon: {
//     width: 24,
//     height: 24,
//   },
//   activeIcon: {
//     tintColor: '#FFFFFF',
//   },
//   tabName: {
//     fontSize: 12,
//     color: '#FFFFFF',
//   },

//   iconContainer: {
//     alignItems: 'center',
//     justifyContent: 'center',
//     borderRadius: 30,
//     width: 40,
//     height: 40,
//   },
// });

// export default BottomBar;

import React, {useState} from 'react';
import {StyleSheet, View, TouchableOpacity, Image, Text} from 'react-native';

const TriosBottomBar = ({
  HomeText,
  EmailText,
  ProfileText,
  tab1Image,
  tab2Image,
  tab3Image,
  tab1Component,
  tab3Component,
  tab2Component,
  tab5Component,
  bottomBarItemStyle,
  bottomBarImageStyle,
  BottomTextStyle,
  bottomBarStyle,
  bottomBarContainerStyle,
  // activeImageStyle,
  useBottomText,
  centerBtnStyle,
  texts,
}) => {
  const [activeTab, setActiveTab] = useState('home');

  const handleTabPress = tabName => {
    setActiveTab(tabName);
  };
  const [htmlState, setHtmlState] = useState('');
  const onhandle = html => {
    switch (html) {
      case 'profile':
        return setHtmlState(
          tab3Component ? tab3Component : defaultTab('Tab 1'),
        );

      case 'home':
        return setHtmlState(tab2Component);
      case 'email':
        return setHtmlState(tab1Component);

      default:
        setHtmlState('home');
    }
    return setHtmlState;
  };
  console.log(htmlState, 'htmlState===>');
  const defaultTab = props => {
    return (
      <View style={{backgroundColor: 'red', flex: 1}}>
        <Text>{props.tabNumber}</Text>
      </View>
    );
  };

  // texts?.map((val, index) => {
  //   emptyArray.push(val?.text);

  //   const onPressAction = () => {
  //     console.log(`${val.text} pressed`);
  //     val.onPress && val.onPress();
  //   };

  //   return (
  //     <TouchableOpacity onPress={onPressAction} key={index}>
  //       <Text>{val.text}</Text>
  //     </TouchableOpacity>
  //   );
  // });

  return (
    <View style={[styles.bottomBarContainer, bottomBarContainerStyle]}>
      <Text>{htmlState}</Text>
      <View style={[styles.bottomBar, bottomBarStyle]}>
        <TouchableOpacity
          style={[styles.bottomBarItem, bottomBarItemStyle]}
          onPress={() => {
            handleTabPress('email');
            onhandle('email');
          }}>
          <Image
            source={
              // tab1Image
              //   ? tab1Image
              //   : {
              //       uri: 'https://cdn4.iconfinder.com/data/icons/social-media-logos-6/512/112-gmail_email_mail-512.png',
              //     }
              tab1Image &&
              typeof tab1Image === 'string' &&
              tab1Image.startsWith('http')
                ? {uri: tab1Image}
                : tab1Image
                ? tab1Image
                : require('../assets/email.png')
            }
            style={[
              styles.bottomBarImage,
              bottomBarImageStyle,
              activeTab === 'email' && [styles.activeImage],
            ]}
          />

          {[
            useBottomText ? (
              <Text key={4} style={[styles.text, BottomTextStyle]}>
                {EmailText ? EmailText : 'Email'}
              </Text>
            ) : null,
          ]}
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.bottomBarItem, bottomBarItemStyle]}
          onPress={() => {
            onhandle('home');
            handleTabPress('home');
          }}>
          <View
            style={[
              {
                backgroundColor: 'white',
                height: 38,
                width: 45,
                borderRadius: 70,
                alignItems: 'center',
                justifyContent: 'center',
                aspectRatio: 1,
              },
              centerBtnStyle,
            ]}>
            <Image
              source={
                // tab2Image
                //   ? tab2Image
                //   : {
                //       uri: 'https://www.bajajallianz.com/content/dam/bagic/home-insurance/My-Home-Insurance.png',
                //     }
                tab2Image &&
                typeof tab2Image === 'string' &&
                tab2Image.startsWith('http')
                  ? {uri: tab2Image}
                  : tab2Image
                  ? tab2Image
                  : require('../assets/home.png')
              }
              style={[
                styles.bottomBarImage,
                bottomBarImageStyle,
                activeTab === 'home' && [styles.activeImage],
              ]}
            />
          </View>
          {[
            useBottomText ? (
              <Text key={3} style={[styles.text, BottomTextStyle]}>
                {HomeText ? HomeText : 'Home'}
              </Text>
            ) : null,
          ]}
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.bottomBarItem, bottomBarItemStyle]}
          onPress={() => {
            handleTabPress('user');
            onhandle('profile');
          }}>
          <Image
            resizeMode="contain"
            source={
              // tab3Image
              //   ? tab3Image
              //   : {
              //       uri: 'https://cdn-icons-png.flaticon.com/512/3135/3135715.png',
              //     }
              tab3Image &&
              typeof tab3Image === 'string' &&
              tab3Image.startsWith('http')
                ? {uri: tab3Image}
                : tab3Image
                ? tab3Image
                : require('../assets/user.png')
            }
            style={[
              styles.bottomBarImage,
              bottomBarImageStyle,
              activeTab === 'user' && [styles.activeImage],
            ]}
          />

          {[
            useBottomText ? (
              <Text key={3} style={[styles.text, BottomTextStyle]}>
                {ProfileText ? ProfileText : 'Profile'}
              </Text>
            ) : null,
          ]}
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  bottomBarContainer: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  bottomBar: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#EB5914',
    height: 53,
    borderTopColor: '#e5e5e5',
    width: '80%',
    alignSelf: 'center',
    marginBottom: 7,
    borderRadius: 10,
    // marginLeft: 30,
    shadowColor: '#EB5914',
    shadowOffset: {
      width: 0,
      height: -3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 5,
    elevation: 7,
    opacity: 1,
  },
  bottomBarItem: {
    alignItems: 'center',
  },
  bottomBarImage: {
    width: 22,
    height: 22,
    marginBottom: 5,
    // tintColor: 'black',
    alignItems: 'center',
    // justifyContent: 'space-between',
    // justifyContent: 'center',
    // marginLeft: '15%',
  },
  activeImage: {
    // tintColor: '#000080',
  },
  text: {
    color: 'black',
    fontSize: 12,
    // marginLeft: '15%',
  },
});

export default TriosBottomBar;
