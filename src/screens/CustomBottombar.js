import React, {useState} from 'react';
import {StyleSheet, View, TouchableOpacity, Image, Text} from 'react-native';

const InitialCustomBottomBar = ({
  HomeText,
  EmailText,
  SettingText,
  SearchText,
  ProfileText,
  tab1Image,
  tab2Image,
  tab3Image,
  tab4Image,
  tab5Image,
  tab1Component,
  tab2Component,
  tab3Component,
  tab4Component,
  tab5Component,
  bottomBarItemStyle,
  bottomBarImageStyle,
  BottomTextStyle,
  bottomBarStyle,
  bottomBarContainerStyle,
  activeImageStyle,
  useBottomText,
}) => {
  const [activeTab, setActiveTab] = useState('home');

  const handleTabPress = tabName => {
    setActiveTab(tabName);
  };
  const [htmlState, setHtmlState] = useState('');
  const onhandle = html => {
    switch (html) {
      case 'profile':
        return setHtmlState(
          tab1Component ? tab1Component : defaultTab('Tab 1'),
        );
      case 'search':
        return setHtmlState(tab2Component);
      case 'home':
        return setHtmlState(tab3Component);
      case 'email':
        return setHtmlState(tab4Component);
      case 'setting':
        return setHtmlState(tab5Component);
      default:
        setHtmlState('home');
    }
    return setHtmlState;
  };
  console.log(htmlState, 'htmlState===>');
  const defaultTab = props => {
    return (
      <View style={{backgroundColor: 'red', flex: 1}}>
        <Text>{props.tabNumber}</Text>
      </View>
    );
  };
  return (
    <View style={[styles.bottomBarContainer, bottomBarContainerStyle]}>
      <Text>{htmlState}</Text>
      <View style={[styles.bottomBar, bottomBarStyle]}>
        <TouchableOpacity
          style={[styles.bottomBarItem, bottomBarItemStyle]}
          onPress={() => {
            handleTabPress('user');
            onhandle('profile');
          }}>
          <Image
            source={
              tab1Image &&
              typeof tab1Image === 'string' &&
              tab1Image.startsWith('http')
                ? {uri: tab1Image}
                : tab1Image
                ? tab1Image
                : require('../assets/user.png')
            }
            style={[
              styles.bottomBarImage,
              bottomBarImageStyle,
              activeTab === 'user' && [styles.activeImage, activeImageStyle],
            ]}
          />
          {[
            useBottomText ? (
              <Text key={1} style={[styles.text, BottomTextStyle]}>
                {ProfileText ? ProfileText : 'Profile'}
              </Text>
            ) : null,
          ]}
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.bottomBarItem, bottomBarItemStyle]}
          onPress={() => {
            handleTabPress('search');
            onhandle('search');
          }}>
          <Image
            source={
              tab2Image &&
              typeof tab2Image === 'string' &&
              tab2Image.startsWith('http')
                ? {uri: tab2Image}
                : tab2Image
                ? tab2Image
                : require('../assets/search-interface-symbol.png')
            }
            style={[
              styles.bottomBarImage,
              bottomBarImageStyle,
              activeTab === 'search' && [styles.activeImage, activeImageStyle],
            ]}
          />
          {[
            useBottomText ? (
              <Text key={2} style={[styles.text, BottomTextStyle]}>
                {SearchText ? SearchText : 'Search'}
              </Text>
            ) : null,
          ]}
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.bottomBarItem, bottomBarItemStyle]}
          onPress={() => {
            onhandle('home');
            handleTabPress('home');
          }}>
          <Image
            source={
              tab3Image &&
              typeof tab3Image === 'string' &&
              tab3Image.startsWith('http')
                ? {uri: tab3Image}
                : tab3Image
                ? tab3Image
                : require('../assets/home.png')
            }
            style={[
              styles.bottomBarImage,
              bottomBarImageStyle,
              activeTab === 'home' && [styles.activeImage, activeImageStyle],
            ]}
          />
          {[
            useBottomText ? (
              <Text key={3} style={[styles.text, BottomTextStyle]}>
                {HomeText ? HomeText : 'Home'}
              </Text>
            ) : null,
          ]}
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.bottomBarItem, bottomBarItemStyle]}
          onPress={() => {
            handleTabPress('email');
            onhandle('email');
          }}>
          <Image
            source={
              tab4Image &&
              typeof tab4Image === 'string' &&
              tab4Image.startsWith('http')
                ? {uri: tab4Image}
                : tab4Image
                ? tab4Image
                : require('../assets/email.png')
            }
            style={[
              styles.bottomBarImage,
              bottomBarImageStyle,
              activeTab === 'email' && [styles.activeImage, activeImageStyle],
            ]}
          />

          {[
            useBottomText ? (
              <Text key={4} style={[styles.text, BottomTextStyle]}>
                {EmailText ? EmailText : 'Email'}
              </Text>
            ) : null,
          ]}
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.bottomBarItem, bottomBarItemStyle]}
          onPress={() => {
            handleTabPress('setting');
            onhandle('setting');
          }}>
          <Image
            source={
              tab4Image &&
              typeof tab4Image === 'string' &&
              tab4Image.startsWith('http')
                ? {uri: tab4Image}
                : tab4Image
                ? tab4Image
                : require('../assets/setting.png')
            }
            style={[
              styles.bottomBarImage,
              bottomBarImageStyle,
              activeTab === 'setting' && [styles.activeImage, activeImageStyle],
            ]}
          />
          {[
            useBottomText ? (
              <Text key={5} style={[styles.text, BottomTextStyle]}>
                {SettingText ? SettingText : ''}
              </Text>
            ) : null,
          ]}
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  bottomBarContainer: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  bottomBar: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#EB5914',
    height: 60,
    borderTopColor: '#e5e5e5',
  },
  bottomBarItem: {
    alignItems: 'center',
  },
  bottomBarImage: {
    width: 22,
    height: 22,
    marginBottom: 5,
  },
  activeImage: {},
  text: {
    color: 'white',
    fontSize: 12,
  },
});

export default InitialCustomBottomBar;
