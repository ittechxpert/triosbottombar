import * as React from 'react';
import {StyleSheet, TouchableOpacity, Text, View} from 'react-native';
import If from './If';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Icon from './Icon';
import {Typography} from '../styles/Typography';

import Theme from '../styles/Theme';
import CustomText from './CustomeText';

const CustomButton = props => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={props.onPress}
      style={[
        props.disabled
          ? {
              ...styles.buttonStyle,
              backgroundColor: Theme.$DIMGRAY,
            }
          : styles.buttonStyle,
        props.buttonStyle,
      ]}
      disabled={props.disabled}>
      <View style={{marginLeft: 5}}>
        <If show={props.useLIcon}>
          <Icon
            name={props.iconLName}
            size={props.iconLSize}
            color={props.iconLColor}
          />
        </If>
      </View>
      <View style={{marginLeft: 5}}>
        <If show={props.useIcon}>
          <Icon
            name={props.iconName}
            size={props.iconSize}
            color={props.iconColor}
            style={{marginRight: '5%'}}
          />
        </If>
      </View>
      <CustomText style={[Typography.medium, props.textStyle]}>
        {props.title}
      </CustomText>
    </TouchableOpacity>
  );
};
CustomButton.defaultProps = {
  disabled: false,
  useIcon: false,
  iconSize: 35,
  iconColor: 'white',
  useLIcon: false,
  iconLSize: 35,
  iconLColor: 'white',
};
const styles = StyleSheet.create({
  buttonStyle: {
    backgroundColor: '#2692DB',
    borderRadius: 30,
    height: hp('6.5%'),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textStyle: {
    textAlign: 'center',
    padding: 10,
  },
});
export default CustomButton;

// shadowColor: '#000',
//         shadowOffset: {
//           width: 2,
//           height: 1,
//         },
//         shadowOpacity: 0.3,
//         shadowRadius: 1.41,
