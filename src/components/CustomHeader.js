import {
  View,
  Text,
  TouchableOpacity,
  SafeAreaView,
  ImageBackground,
  Image,
  Platform,
} from 'react-native';
import React, {useState} from 'react';
import {Header} from '@react-navigation/elements';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from './Icon';
import Theme from '../styles/Theme';
import CustomText from './CustomeText';
import {Typography} from '../styles/Typography';
import {Utils} from '../utils/Utils';
import If from './If';
import {useNavigation} from '@react-navigation/native';

const CustomHeader = ({
  onPress,
  IconName,
  text,
  Skip,
  useIconImage,
  useIcon,
  IconLeft,
  onPressLeft,
}) => {
  const navigation = useNavigation();

  return (
    <View>
      <Header
        headerStyle={{
          ...Platform.select({
            ios: {
              backgroundColor: Theme.$ORANGE,
              height: hp('16'),
              borderBottomLeftRadius: wp('6'),
              borderBottomRightRadius: wp('6'),
            },
            android: {
              backgroundColor: Theme.$WHITE_COLOR,
              height: hp('10'),
              // borderBottomLeftRadius: wp('8'),
              // borderBottomRightRadius: wp('8'),
              shadowOpacity: 70,
              elevation: 50,
              //   backgroundColor: 'black',
              shadowRadius: 3,
            },
          }),
        }}
        headerTitleAlign="center"
        headerTitle={() => {
          return (
            <React.Fragment>
              <CustomText
                style={[
                  Typography.title,
                  {
                    ...Platform.select({
                      ios: {
                        color: '#fff',
                        fontWeight: '400',
                        fontSize: wp('4.5%'),
                        marginBottom: wp('8'),
                      },
                      android: {
                        color: '#fff',
                        fontWeight: '400',
                        fontSize: wp('4.5%'),
                        marginBottom: hp('2'),
                      },
                    }),
                  },
                ]}>
                {text}
              </CustomText>
            </React.Fragment>
          );
        }}
        headerLeft={() => {
          return (
            <TouchableOpacity
              style={{
                ...Platform.select({
                  ios: {
                    paddingLeft: 20,
                    marginBottom: wp('8'),
                  },
                  android: {
                    paddingLeft: 20,
                    marginBottom: hp('2'),
                  },
                }),
              }}
              onPress={onPress}>
              <If show={useIcon}>
                <Icon size={30} color="#fff" name={IconName} />
              </If>
              <If show={useIconImage}>
                <TouchableOpacity onPress={onPressLeft}>
                  <Image resizeMode="cover" source={IconLeft} />
                </TouchableOpacity>
              </If>
            </TouchableOpacity>
          );
        }}
        headerRight={() => {
          return (
            <View
              style={{
                ...Platform.select({
                  ios: {
                    color: '#fff',
                    paddingRight: wp(5),
                    marginBottom: wp('8'),
                  },
                  android: {
                    color: '#fff',
                    paddingRight: wp(5),
                    marginBottom: hp('2'),
                  },
                }),
              }}>
              <Image
                style={{
                  width: wp('8'),
                  resizeMode: 'contain',
                }}
                source={Utils.Img.RLOGO}
              />
            </View>
          );
        }}
      />
      <View style={{backgroundColor: Theme.$DARkGREY}}></View>
    </View>
  );
};

export default CustomHeader;

// 'chevron-back-outline'
