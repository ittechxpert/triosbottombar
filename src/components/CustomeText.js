import * as React from 'react';
import {Text, StyleSheet, TextStyle} from 'react-native';
import {Typography} from '../styles/Typography';

const CustomText = props => {
  return (
    <Text
      ellipsizeMode={props?.ellipsizeMode}
      numberOfLines={props.numberOfLines}
      allowFontScaling={false}
      style={[Styles.text, props.style, {margin: 0, padding: 0}]}>
      {props.children}
    </Text>
  );
};

export default CustomText;

const Styles = StyleSheet.create({
  text: {
    ...Typography.heading,
    color: '#000',
  },
});
